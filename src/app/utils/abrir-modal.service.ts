import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AbrirModalService {

  constructor(private dialog: MatDialog) { }

  mostrarComponente(componenteAMostrar, data?: any, widht: number = 600): Observable<any> {
    return this.dialog.open(
      componenteAMostrar,
      { data, width: `${widht}px`, }
    ).afterClosed();
  }
}
