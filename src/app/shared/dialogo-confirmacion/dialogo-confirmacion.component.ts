import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogoSimpleModel } from './dialog-confirmacion.model';

@Component({
  selector: 'app-dialogo-confirmacion',
  templateUrl: './dialogo-confirmacion.component.html',
  styleUrls: ['./dialogo-confirmacion.component.scss']
})
export class DialogoConfirmacionComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogoConfirmacionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogoSimpleModel
  ) { }

  ngOnInit() {
  }

  onAceptacion(): void {
    return this.dialogRef.close({ acepto: true, cancelo: false });
  }

  onCancelacion(): void {
    return this.dialogRef.close({ acepto: false, cancelo: true });
  }

}
