import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogoSimpleModel } from './dialog-confirmacion.model';
import { Observable } from 'rxjs';
import { DialogoConfirmacionComponent } from './dialogo-confirmacion.component';

@Injectable({
  providedIn: 'root'
})
export class DialogoConfirmacionService {

  constructor(private dialog: MatDialog) { }

  mostrarDialogo(msj: DialogoSimpleModel): Observable<any> {
    msj.opcionAceptar = !msj.opcionAceptar ? 'Aceptar' : msj.opcionAceptar;
    const dialogRef = this.dialog.open(DialogoConfirmacionComponent, { data: msj });
    dialogRef.disableClose = true;
    return dialogRef.afterClosed();
  }

}
